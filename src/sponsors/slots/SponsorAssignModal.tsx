import React, {useState} from 'react'
import {
  Button,
  ButtonGroup,
  Dropdown,
  DropdownButton,
  FormControl,
  InputGroup,
  Modal
} from 'react-bootstrap'
import {InputField} from '../../form/InputField'
import {SelectCallback} from 'react-bootstrap/helpers'
import './SponsorAssignModal.scss'
import {
  Slot,
  Sponsor,
  SponsorSlot
} from '../../main/types'

type Props = {
  assign: SponsorSlot,
  sponsors: Sponsor[]
  onChangeSponsor: (sponsor: Sponsor) => void,
  onChangeDonation: (event: React.ChangeEvent<HTMLInputElement>) => void,
  onChangeSlot: (slot: Slot) => void,
  onChangePaymentReceived: (event: React.ChangeEvent<HTMLInputElement>) => void,
  onKeyPress?: () => void,
  onClose: () => void,
  onSave: () => Promise<void>
}
export const SponsorAssignModal = (props: Props) => {
  const [selected, setSelected] = useState<Sponsor | null>(props.sponsors.find(s => s.name === props.assign?.name) ?? null)
  const [slotSelected, setSlotSelected] = useState<Slot | null>(props.assign?.slot ?? null)

  const handleSelectSponsor: SelectCallback = async (selected: string | null) => {
    const sponsor: Sponsor | undefined = selected ? props.sponsors.find(s => s.index === parseInt(selected)) : undefined
    setSelected(sponsor ?? null)
    if (sponsor !== undefined) {
      props.onChangeSponsor(sponsor)
    }
  }

  const handleSelectSlot: SelectCallback = async (slot: string | null) => {
    setSlotSelected(slot as Slot)
    props.onChangeSlot(slot as Slot)
  }

  const keyPressHandler = props.onKeyPress ?? (() => {
  })

  return <Modal show={props.assign !== null} onHide={props.onClose}>
    <Modal.Header closeButton>
      <Modal.Title>Assign sponsor</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <InputGroup className="mb-3 sponsorDropDown">
        <InputGroup.Prepend>
          <InputGroup.Text id={'fieldSponsor'} className="label">Sponsor:</InputGroup.Text>
        </InputGroup.Prepend>
        <DropdownButton as={ButtonGroup}
                        variant="info"
                        onSelect={handleSelectSponsor}
                        title={selected !== null ? selected.name
                                                 : 'Select one'}>
          {props.sponsors.map(sponsor =>
                  <Dropdown.Item key={sponsor.index} eventKey={'' + sponsor.index}
                                 active={sponsor.index === selected?.index}>
                    <Dropdown.ItemText className="text-sub">{sponsor.name}</Dropdown.ItemText>
                  </Dropdown.Item>
          )}
        </DropdownButton>
      </InputGroup>
      <InputGroup className="mb-3 slotDropDown">
        <InputGroup.Prepend>
          <InputGroup.Text id={'fieldSlot'} className="label">Slot:</InputGroup.Text>
        </InputGroup.Prepend>
        <DropdownButton as={ButtonGroup}
                        variant="info"
                        onSelect={handleSelectSlot}
                        title={slotSelected !== null ? slotSelected
                                                     : 'Select one'}>
          {['Gold', 'Silver', 'Bronze'].map(slot =>
                  <Dropdown.Item key={slot} eventKey={slot}
                                 active={slot === slotSelected}>
                    <Dropdown.ItemText className="text-sub">{slot}</Dropdown.ItemText>
                  </Dropdown.Item>
          )}
        </DropdownButton>
      </InputGroup>
      <InputField className="mb-3" label={'Donation:'} value={props.assign?.donation ?? ''}
                  placeholder={'1000€'}
                  onChange={props.onChangeDonation} onKeyPress={keyPressHandler}/>
      <InputGroup className={'mb-3'}>
        <InputGroup.Prepend>
          <InputGroup.Text id={'payment'} className="label">{'Payment Received?:'}</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl checked={props.assign.paymentReceived || undefined}
                     type={'checkbox'}
                     aria-label={'Payment Received?:'}
                     aria-describedby={'payment'}
                     onChange={props.onChangePaymentReceived}
        />
      </InputGroup>
    </Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onClick={props.onClose}>Close</Button>
      <Button variant="info" onClick={props.onSave}>Save changes</Button>
    </Modal.Footer>
  </Modal>
}