import {Price} from '../main/types'


export const parsePrice = (price: Price): number => {
  return parseFloat(price.replace(',', '.'))
}

export const formatEuro = (amount: number): Price => {
  let total: string = amount.toFixed(2)
  //if (total.endsWith('.00')) total = total.substring(0, total.length - 3)
  total = total.replace('.', ',')
  return total + '€'
}