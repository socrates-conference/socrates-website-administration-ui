export type UUID = string
export type Identifier = string

export type Iterable = { [key: string]: any }

export type TableRow = { index?: number }

export type Entity = {
  id: UUID
}

export type RoomTypeName = Identifier & "new" | "single" | "double-shared" | "junior-double" | "junior-double-shared"

export type RoomType = Iterable & {
  type: RoomTypeName,
  description: string,
  pricePerNight: string,
  beds: number,
  count: number
}
export type FlatFee = Iterable & {
  type: Identifier,
  description: string,
  price: string,
  excludeFromSponsoring?: boolean
}

export enum ConferenceState {
  PREPARATION = 'PREPARATION',
  REGISTRATION = 'REGISTRATION',
  IN_PROGRESS = 'IN_PROGRESS',
  CLEANUP = 'CLEANUP',
  CONCLUDED = 'CONCLUDED'
}

export type Conference = Iterable & TableRow & Entity & {
  byline?: string,
  diversityRatio?: number,
  endDate?: string,
  flatFees?: FlatFee[]
  location: string,
  maxSeatsPerSponsor?: number,
  roomTypes?: RoomType[],
  startDate?: string,
  state?: ConferenceState,
  title: string,
  year: number,
}
export type Template = TableRow & {
  title: string,
  subject: string,
  htmlText?: string,
  text: string
}

export type HistoryEntry = TableRow & {
  date: string,
  time: string,
  template: string,
  username?: string,
  email?: string
}

export type Subscriber = TableRow & Entity & {
  email: string,
  name: string
}

export type Applicant = TableRow & Entity & {
  conferenceId: UUID,
  email: string,
  name: string,
  roomTypeSelected: string[],
  roommate?: string,
  diversitySelected: string
}

export enum BillingStatus {
  NOT_INVOICED = "NOT_INVOICED",
  INVOICED = "INVOICED",
  PAID = "PAID",
  OVERDUE = "OVERDUE"
}

export type AttendeeReason = undefined | 'staff' | 'sponsor' | 'trainer'

export type Address = {
  firstname?: string,
  lastname?: string,
  address1?:string,
  address2?:string,
  postal?:string,
  city?:string,
  country?:string,
}

export type BillingAddress = Address & {
  company?: string,
  vat?: string,
}

export type Attendee = TableRow & Entity & {
  conferenceId: UUID,
  email: string,
  billingStatus?: BillingStatus,
  personalAddress?: Address,
  billingAddress?: BillingAddress,
  confirmationKey?: string,
  nickname: string,
  reason?: AttendeeReason,
  diversitySelected: string,
  booking: {
    email: string
    room: RoomOccupancy
    packages: Package[],
    swag?: boolean,
    swagSize?: string,
    dietary?: string
  }
}

export type Sponsor = TableRow & Entity & {
  name: string,
  url: string,
  contactName: string;
  contactEmail: string,
  contactPhone: string,
  logo: string
}

export type Slot = 'Platinum' | 'Gold' | 'Silver' | 'Bronze'

export type SponsorSlot = TableRow & Entity & {
  conference: Identifier,
  logo: string,
  url: string,
  name: string,
  slot: Slot,
  donation: string,
  paymentReceived: boolean
}

export type Price = string

export type Stats = {
  attendeeCount: number
  attendeesConfirmed: number
  sponsoringAmount: Price
  expensesAmount: Price
  bookedPackages: Record<Package, number>
  bookedBeds: Record<string, number>
  bookedBedsDiversity: Record<string, number>
}

export type Day = Identifier & 'Wed' | 'Thu' | 'Fri' | 'Sat' | 'Sun'
export type Package = Identifier & 'conference' | 'training' | 'workshops'

export type RoomOccupancy = {
  roomType: RoomTypeName
  email: string
  roommate?: string
  diversity: boolean
  family: boolean
  daysSelected: Day[]
}

export type Booking = TableRow & {
  email: string
  room: RoomOccupancy
  packages: Package[],
  swag?: boolean,
  swagSize?: string,
  dietary?: string
}

export type LotteryInput = {
  conferenceId: UUID,
  bedsToAllocate: Record<string, number>
  diversityPercentage: number
}