import React, {
  createContext,
  PropsWithChildren,
  useContext,
  useState
} from 'react'
import {
  signIn,
  signOut
} from './api/login'

type AuthContext = {
  user: string
  signIn: (username: string, password: string) => Promise<any>,
  signOut: () => Promise<void>
}
const DEFAULT_CONTEXT: AuthContext = {
  user: '',
  signIn: async (username: string, password: string) => console.log('Not logging in: ', username, password),
  signOut: async () => console.log('not logging out')
}
const authContext = createContext(DEFAULT_CONTEXT)
export const useAuth = () => useContext(authContext)

const useProvideAuth = () => {
  const [user, setUser] = useState('')
  const signin = async (username: string, password: string) => {
    return signIn(username, password)
            .then(info => {
              setUser(info.username)
              return info
            })
  }

  const signout = async () => {
    return signOut()
            .then(() => setUser(''))
  }

  return {
    user,
    signIn: signin,
    signOut: signout
  }
}

export const AuthProvider = ({children}: PropsWithChildren<any>) => {
  const auth = useProvideAuth()
  return <authContext.Provider value={auth}>{children}</authContext.Provider>
}