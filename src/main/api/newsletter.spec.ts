import {compareHistoryEntries} from './newsletter'

describe('compareHistoryEntries:', () => {
  it('should return the correct order', () => {
    expect([
      {email: 'a', username: 'a', template: 'a', date: 'Sun 01.01.2023', time: '12:00:00'},
      {email: 'b', username: 'b', template: 'b', date: 'Wed 01.02.2023', time: '12:00:00'}
    ].sort(compareHistoryEntries)).toEqual([
      {email: 'b', username: 'b', template: 'b', date: 'Wed 01.02.2023', time: '12:00:00'},
      {email: 'a', username: 'a', template: 'a', date: 'Sun 01.01.2023', time: '12:00:00'}
    ])

    expect([
      {email: 'a', username: 'a', template: 'a', date: 'Wed 01.02.2023', time: '12:00:00'},
      {email: 'b', username: 'b', template: 'b', date: 'Sun 01.01.2023', time: '12:00:00'}
    ].sort(compareHistoryEntries)).toEqual([
      {email: 'a', username: 'a', template: 'a', date: 'Wed 01.02.2023', time: '12:00:00'},
      {email: 'b', username: 'b', template: 'b', date: 'Sun 01.01.2023', time: '12:00:00'}])

    expect([
      {email: 'a', username: 'a', template: 'a', date: 'Sun 01.01.2023', time: '12:00:00'},
      {email: 'b', username: 'b', template: 'b', date: 'Sun 01.01.2023', time: '12:00:01'}
    ].sort(compareHistoryEntries)).toEqual([
      {email: 'b', username: 'b', template: 'b', date: 'Sun 01.01.2023', time: '12:00:01'},
      {email: 'a', username: 'a', template: 'a', date: 'Sun 01.01.2023', time: '12:00:00'}])

    expect([
      {email: 'a', username: 'a', template: 'a', date: 'Wed 01.01.2023', time: '12:00:01'},
      {email: 'b', username: 'b', template: 'b', date: 'Wed 01.01.2023', time: '12:00:00'}
    ].sort(compareHistoryEntries)).toEqual([
      {email: 'a', username: 'a', template: 'a', date: 'Wed 01.01.2023', time: '12:00:01'},
      {email: 'b', username: 'b', template: 'b', date: 'Wed 01.01.2023', time: '12:00:00'}])
  })
})
