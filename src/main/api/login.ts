import {Auth} from 'aws-amplify'
import axios from 'axios'

const handleSuccess = (info: any) => {
  axios.defaults.headers.common["Authorization"] = info.signInUserSession.idToken.jwtToken
  console.log('Successfully logged in:', info)
}

export const signIn = (username: string, password: string) => {
  return Auth.signIn(username, password)
    .then(info => {
      handleSuccess(info)
      return info
    })
}

export const signOut = () => {
  return Auth.signOut()
}
