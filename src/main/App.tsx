import React from 'react'
import './App.scss'
import {Amplify} from 'aws-amplify'
import config from '../aws-exports'
import {LoginScreen} from '../login/LoginScreen'
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
  useLocation
} from 'react-router-dom'
import {NewsletterScreen} from '../newsletter/NewsletterScreen'
import {EventsScreen} from '../events/EventsScreen'
import {ConferenceProvider} from './ConferenceProvider'
import {
  AuthProvider,
  useAuth
} from './AuthProvider'
import {NewsletterProvider} from './NewsletterProvider'
import {SponsorsScreen} from '../sponsors/SponsorsScreen'
import {SponsorsProvider} from './SponsorsProvider'
import {Subscribers} from './api/subscribers'
import {Sponsors} from './api/sponsors'
import {Templates} from './api/templates'
import {Newsletter} from './api/newsletter'
import {Conferences} from './api/conferences'
import {ApplicantsProvider} from './ApplicantsProvider'
import {Applicants} from './api/applicants'
import {Attendees} from './api/attendees'
import {AttendeesProvider} from './AttendeesProvider'
import {BookingImpl} from './api/booking'
import {BookingProvider} from './BookingProvider'

Amplify.configure({
  Auth: {
    region: config.aws_cognito_region,
    userPoolId: config.aws_user_pools_id,
    userPoolWebClientId: config.aws_user_pools_web_client_id,
    identityPoolId: config.aws_cognito_identity_pool_id
  }
})

const PrivateRoute = ({children, path}: any) => {
  const auth = useAuth()
  return <Route
          path={path}
          render={
            ({location}) =>
                    auth.user && auth.user.trim() !== '' ? children :
                            <Redirect to={{pathname: '/login', state: {from: location}}}/>
          }/>
}

const NoMatch = () => {
  const location = useLocation()
  return <div>
    <h3>No Match for route {location.pathname}.</h3>
  </div>
}

const App = () => {
  return <AuthProvider>
    <ConferenceProvider
            conferencesApi={new Conferences()}>
      <Router>
        <Switch>
          <Route exact path="/">
            <Redirect to="/events"/>
          </Route>
          <Route path="/login">
            <LoginScreen/>
          </Route>
          <PrivateRoute path="/newsletter">
            <NewsletterProvider
                    subscribersApi={new Subscribers()}
                    templatesApi={new Templates()}
                    newsletterApi={new Newsletter()}>
              <NewsletterScreen/>
            </NewsletterProvider>
          </PrivateRoute>
          <SponsorsProvider sponsorsApi={new Sponsors()}>
            <PrivateRoute path="/sponsors">
              <SponsorsScreen/>
            </PrivateRoute>
            <PrivateRoute path="/events">
              <BookingProvider bookingApi={new BookingImpl()}>
                <ApplicantsProvider applicantsApi={new Applicants()}>
                  <AttendeesProvider attendeesApi={new Attendees()}>
                    <EventsScreen/>
                  </AttendeesProvider>
                </ApplicantsProvider>
              </BookingProvider>
            </PrivateRoute>
          </SponsorsProvider>
          <Route path="*">
            <NoMatch/>
          </Route>
        </Switch>
      </Router>
    </ConferenceProvider>
  </AuthProvider>
}

export default App
