import {
  TemplatesApi
} from '../api/templates'
import {Template} from '../types'

export class LocalTemplates implements TemplatesApi {
  private _templates: Template[]

  public constructor() {
    this._templates = []
  }

  public createTemplate = async () => {
    const created: Template = {title: '', subject:'', text: '', htmlText: '', index: this._templates.length}
    this._templates = [...this._templates, created]
    return created
  }

  public deleteTemplate = async (index: number) => {
    const updated = [...this._templates]
    updated.splice(index, 1)
    this._templates = updated
  }

  public updateTemplate = async (template: Template) => {
    const updated = [...this._templates]
    updated[template.index ?? updated.length] = template
    this._templates = updated
  }

  public getTemplates = async () => {
    return [...this._templates]
  }
}