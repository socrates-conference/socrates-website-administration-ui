
import {
  SubscribersApi
} from '../api/subscribers'
import {Subscriber} from '../types'

export class LocalSubscribers implements SubscribersApi {
  private _subscribers: Subscriber[]

  constructor() {
    this._subscribers = [
      {email: 'john@doe.com', name: 'John Doe', index: 0, id: "0"},
      {email: 'some@dude.com', name: 'Some Dude', index: 1, id: "1"}
    ]
  }

  public deleteSubscriber = async (index?: number) => {
    this._subscribers = [...this._subscribers.filter((v, i) => i !== index)]
  }

  public getSubscribers = async () => {
    return [...this._subscribers]
  }
}



