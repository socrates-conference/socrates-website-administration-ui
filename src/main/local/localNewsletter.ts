import moment from 'moment'
import {
  NewsletterApi
} from '../api/newsletter'
import {HistoryEntry} from '../types'

export class LocalNewsletter implements NewsletterApi {
  private _history: HistoryEntry[]

  constructor() {
    this._history = []
  }

  public send = async (title: string) => {
    const date: string = moment().format('ddd DD.MM.YYYY')
    const time: string = moment().format('HH:mm:ss')
    const entry: HistoryEntry = {index: this._history.length, date, time, template: title}
    this._history = [...this._history, entry]
  }

  public getHistory = async () => {
    return [...this._history]
  }
}