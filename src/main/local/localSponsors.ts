import {v4 as uuid} from 'uuid'
import {SponsorsApi} from '../api/sponsors'
import {
  Sponsor,
  SponsorSlot
} from '../types'

export class LocalSponsors implements SponsorsApi {
  private _sponsors: Sponsor[]
  private _sponsorSlots: SponsorSlot[]

  public constructor() {
    const sponsor: Sponsor = {
      index: 0,
      id: uuid(),
      logo: '/images/logos/codecentric-160.png',
      url: 'http://www.codecentric.de',
      name: 'codecentric',
      contactName: 'Claudia Fröhling',
      contactEmail: 'claudia.froehling@codecentric.de',
      contactPhone: ''
    }
    this._sponsors = [sponsor]
    this._sponsorSlots = []
  }

  public createSponsor = async () => {
    const created: Sponsor = {
      id: uuid(),
      index: this._sponsors.length,
      name: '',
      url: '',
      contactName: '',
      contactEmail: '',
      contactPhone: '',
      logo: ''
    }
    this._sponsors = [...this._sponsors, created]
    return created
  }
  public updateSponsor = async (updated: Sponsor) => {
    const index: number | undefined = updated.index
    if (index !== undefined) {
      const updatedList: Sponsor[] = [...this._sponsors]
      updatedList[index] = updated
      this._sponsors = updatedList
    }
  }

  public deleteSponsor = async (index?: number) => {
    if (index !== undefined) {
      this._sponsors = [...this._sponsors].filter(s => s.index !== index)
    }
  }

  public getSponsors = async () => {
    return [...this._sponsors]
  }

  public getSponsorSlots = async () => {
    return [...this._sponsorSlots]
  }

  public assignSponsorSlot = async (slot: SponsorSlot) => {
    slot.index = this._sponsorSlots.length
    this._sponsorSlots = [...this._sponsorSlots, slot]
  }

  public updateSponsorSlot = async (slot: SponsorSlot) => {
    if (slot.index !== undefined) {
      this._sponsorSlots[slot.index] = slot
    }
  }

  public deleteSponsorSlot = async (conference: string, index?: number) => {
    this._sponsorSlots = this._sponsorSlots.filter(s => s.index !== index).map((s, index) => ({...s, index}))
  }

  public upload = async() => {
  }
}