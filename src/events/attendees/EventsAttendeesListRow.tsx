import React, {
  PropsWithChildren,
  useRef,
  useState
} from 'react'
import {
  Button,
  Overlay,
  OverlayTrigger,
  Popover,
  Tooltip
} from 'react-bootstrap'
import {
  FaCheckCircle,
  FaExclamationTriangle,
  FaRecycle,
  FaTrash,
  FaUsers,
  HiCurrencyDollar,
  MdEdit,
  MdMail,
  MdMarkunreadMailbox
} from 'react-icons/all'
import {
  Attendee,
  BillingStatus,
  UUID
} from '../../main/types'

const normalized = (str?: string) => str?.trim().toLowerCase() ?? ''

type DiversityStatusProps = {
  diversity: string
}

function DiversityStatus({diversity}: DiversityStatusProps) {

  const renderTooltip = (props: any) => (
          <Tooltip id="button-tooltip" {...props}>
            {diversity}
          </Tooltip>
  )
  const target = useRef(null)
  // noinspection RequiredAttributes
  const icon = normalized(diversity) === 'yes'
          ? <FaCheckCircle className={'text-success'}/>
          : normalized(diversity) === 'no' ? '' :
                  // @ts-ignore  Type of target attribute is inaccurate
                  <OverlayTrigger target={target.current}
                          placement="right"
                          delay={{show: 100, hide: 200}}
                          overlay={renderTooltip}
                          // @ts-ignore  Type FaCheckCircle won't accept a Ref, but the rendered element does.
                  ><FaCheckCircle ref={target} className={'text-warning'}/></OverlayTrigger>
  return <>{icon}</>
}

type EventsAttendeesListRowProps = {
  attendee: Attendee,
  sharers: string[],
  onDelete: (id?: UUID) => void,
  onReset: (email?: string) => void,
  onResend: (email?: string) => void
  onEdit: (email: string) => void
}

type DeleteAttendeeButtonProps = {
  attendee: Attendee,
  onDelete: (id?: UUID) => void
}

const DeleteAttendeeButton = ({attendee, onDelete}: PropsWithChildren<DeleteAttendeeButtonProps>) => {
  const [show, setShow] = useState(false)
  const [target, setTarget] = useState<HTMLElement>()

  return <>
    <Button className="p-0" variant="none" title={`Delete
    ${attendee.nickname} from the list`}
            onClick={(e) => {
              setTarget(e.currentTarget)
              setShow(!show)
            }}>
      <FaTrash/>
    </Button>
    {target &&
            // <!--suppress RequiredAttributes -->
            <Overlay show={show} target={target} rootClose={true}>
              <Popover id={'delete-overlay'} className="popover-positioned-top" title="Cancel Participation">
                <Popover.Title as="h3">Are you sure you want to remove:</Popover.Title>
                <Popover.Content>
                  <h4>{attendee.nickname}</h4>
                  <p>from the attendee list? This cannot be undone.</p>
                  <Button className="mr-2" variant={'success'}
                          onClick={() => {
                            onDelete(attendee.id)
                            setShow(false)
                          }}>Yes</Button>
                  <Button variant={'danger'}
                          onClick={() =>
                                  setShow(false)
                          }>No</Button>
                </Popover.Content>
              </Popover>
            </Overlay>}
  </>
}

type ResetAccountButtonProps = {
  attendee: Attendee,
  onReset: (email?: string) => void
}

const ResetAccountButton = ({attendee, onReset}: PropsWithChildren<ResetAccountButtonProps>) => {
  const [show, setShow] = useState(false)
  const [target, setTarget] = useState<HTMLElement>()

  return <>
    <Button className="p-0" variant="none" title={`Reset account for ${attendee.nickname}`}
            onClick={(e) => {
              setTarget(e.currentTarget)
              setShow(!show)
            }}>
      <FaRecycle/>
    </Button>
    {target &&
            // <!--suppress RequiredAttributes -->
            <Overlay show={show} target={target} rootClose={true}>
              <Popover id={'delete-overlay'} className="popover-positioned-top" title="Cancel Participation">
                <Popover.Title as="h3">Reset the user account for:</Popover.Title>
                <Popover.Content>
                  <h4>{attendee.nickname}?</h4>
                  <p>This cannot be undone.</p>
                  <Button className="mr-2" variant={'success'}
                          onClick={() => {
                            onReset(attendee.email)
                            setShow(false)
                          }}>Yes</Button>
                  <Button variant={'danger'}
                          onClick={() =>
                                  setShow(false)
                          }>No</Button>
                </Popover.Content>
              </Popover>
            </Overlay>}
  </>
}

type BillingStatusIconProps = {
  billingStatus: BillingStatus
}
const BillingStatusIcon = ({billingStatus}: BillingStatusIconProps) => {
  console.log(billingStatus)
  return <>
    {billingStatus === BillingStatus.NOT_INVOICED ? <HiCurrencyDollar fontSize={"24"} className={'text-muted'}/>
            : billingStatus === BillingStatus.INVOICED ? <HiCurrencyDollar fontSize={"24"} className={'text-warning'}/>
                    : billingStatus === BillingStatus.PAID ? <HiCurrencyDollar fontSize={"24"} className={'text-success'}/>
                            : billingStatus === BillingStatus.OVERDUE ? <HiCurrencyDollar fontSize={"24"} className={'text-danger'}/>
                                    : <FaExclamationTriangle fontSize={"24"} className={'text-danger'}/>}
  </>

}

const extractName = (attendee: Attendee) => {
  if (attendee.personalAddress?.lastname){
    return attendee.personalAddress.lastname + ', ' + attendee.personalAddress.firstname
  }

  if (attendee.billingAddress?.lastname){
    return attendee.billingAddress.lastname + ', ' + attendee.billingAddress.firstname
  }

  return ''
}

export const EventsAttendeesListRow = ({
  attendee,
  sharers,
  onDelete,
  onReset,
  onResend,
  onEdit
}: EventsAttendeesListRowProps) => {
  const name = extractName(attendee)
  const booking = attendee.booking
  const roommate = booking?.room?.roommate ?? ''
  return <tr>
    <td className="text-left align-baseline">{(attendee.index || 0) + 1}</td>
    <td className="text-left align-baseline">{attendee.nickname}</td>
    <td className="text-left align-baseline">{name}</td>
    <td className="text-left align-baseline">{attendee.email} {(attendee.confirmationKey === undefined && name.trim() !== '') ?
            <FaCheckCircle className={'text-success'}/> : <FaExclamationTriangle className={'text-danger'}/>}</td>
    <td className="text-center align-baseline">{attendee.confirmationKey !== undefined ? attendee.confirmationKey :
            <BillingStatusIcon billingStatus={attendee.billingStatus ?? BillingStatus.NOT_INVOICED}/>}</td>
    <td className="text-left align-baseline">{booking?.room?.roomType ?? 'single'}</td>
    <td className="text-center align-baseline"><DiversityStatus diversity={booking?.room?.diversity ? 'yes' : 'no'}/>
    </td>
    <td className="text-left align-baseline">{roommate}{roommate !== '' && (sharers.indexOf(roommate) > -1 ?
            <FaCheckCircle className="text-success"/> : <FaExclamationTriangle className="text-danger"/>)}</td>
    <td className="text-left align-baseline">{booking?.room?.family && <FaUsers className={'text-info'}/>}</td>
    <td className="text-left align-baseline">{attendee.reason ?? 'attendee'}</td>
    <td className="text-center align-baseline actions-column">
      <Button className="p-0 mr-1" variant="none" role="link" href={`mailto:${attendee.email}`}
              title={`Send an email to ${attendee.nickname}`}>
        <MdMail/>
      </Button>
      <Button className='p-0 mr-1' variant='none' role='link' onClick={() => onEdit(attendee.email)}>
        <MdEdit/>
      </Button>
      <Button className="p-0 mr-1" variant="none" role="link" onClick={() => onResend(attendee.email)}
              title={`Resend confirmation email to ${attendee.nickname}`}>
        <MdMarkunreadMailbox/>
      </Button>
      <ResetAccountButton attendee={attendee} onReset={onReset}/>
      <DeleteAttendeeButton
              attendee={attendee} onDelete={onDelete}/>
    </td>
  </tr>
}

export const EmptyEventsAttendeesListRow = () => <tr>
  <td className="text-center" colSpan={11}>There are currently no active attendees.</td>
</tr>