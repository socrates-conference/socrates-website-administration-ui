import React, {
  useEffect,
  useState
} from 'react'
import './EventsAttendeesList.scss'
import {
  PagingTable,
  RowMapper
} from '../../table/PagingTable'
import {
  EmptyEventsAttendeesListRow,
  EventsAttendeesListRow
} from './EventsAttendeesListRow'

import {
  Attendee,
  Conference,
  Package,
  RoomOccupancy,
  UUID,
} from '../../main/types'
import {AttendeeUpdate, useAttendees} from '../../main/AttendeesProvider'
import {useConference} from '../../main/ConferenceProvider'
import {useBooking} from '../../main/BookingProvider'
import {ModalMessage} from '../../modal/ModalMessage'
import {MessageType} from '../../modal/messages'
import { EventsAttendeeEditModal } from './EventsAttendeeEditModal'

const AttendeesTableHeader = () => <tr>
  <th>#</th>
  <th>Nickname</th>
  <th>Name</th>
  <th>Email</th>
  <th>Confirmed / Invoice</th>
  <th>Room type</th>
  <th>Diversity</th>
  <th>Roommate</th>
  <th>Family?</th>
  <th>Status</th>
  <th>Actions</th>
</tr>

const AttendeesHeader = () => <div>
  <h5 className="mb-0">Event attendees</h5>
</div>

export const EventsAttendeesList = () => {
  const conferenceContext = useConference()
  const attendeeContext = useAttendees()
  const bookingContext = useBooking()
  const [conference, setConference] = useState<Conference | undefined>()
  const [attendees, setAttendees] = useState<Attendee[]>([])

  const [editAttendee, setEditAttendee] = useState<Attendee | null>(null)

  const [messageType, setMessageType] = useState<MessageType | undefined>(undefined)
  const [message, setMessage] = useState<string>('')

  useEffect(() => {
    let isSubscribed = true
    if (!conference) {
      if (conferenceContext.current)
        setConference(conferenceContext.current)
      else {
        conferenceContext.choices()
                .then(c => {
                  if (isSubscribed) setConference(c[0])
                })
      }
    } else {
      attendeeContext.getAttendees(conference.id)
              .then(a => {
                if (isSubscribed) {
                  setAttendees(a)
                }
              })
    }
    return () => {
      isSubscribed = false
    }
  }, [attendeeContext, bookingContext, conferenceContext, conference])

  const onDelete = async (id?: UUID) => {
    await attendeeContext.deleteAttendee(id)
    if (conference) setAttendees(await attendeeContext.getAttendees(conference.id))
  }

  const onReset = async (email?: string) => {
    if (conference) {
      await attendeeContext.resetAccount(conference.id, email)
      setAttendees(await attendeeContext.getAttendees(conference.id))
    }
  }

  const onResend = async (email?: string) => {
    if (conference) {
      await attendeeContext.resendConfirmation(conference.id, email)
              .catch(e => {
                setMessageType(MessageType.RESEND_FAILED)
                if (e.message.endsWith('409')) {
                  setMessage('The account has already been confirmed.')
                } else {
                  setMessage('Resending the confirmation mail failed due to: ' + e.message)
                }
                return true
              })
              .then((error) => {
                if (!error) {
                  setMessageType(MessageType.RESEND_SUCCESS)
                  setMessage('The confirmation email was successfully resent to the attendee.')
                }
              })
    }
  }

  const onEdit = (email: string) => {
    if (conference) {
      const attendee = attendees.find(a => a.email === email)
      if (attendee) {
        setEditAttendee(attendee)
      }
    }
  }

  const onAttendeeEditClose = () => {
    setEditAttendee(null)
  }

  const onAttendeeDetailsChanged = async (email: string, update: AttendeeUpdate) => {
    if (conference) {
      await attendeeContext.changeAttendeeDetails(conference.id, email, update)
      setEditAttendee(null)
      setAttendees(await attendeeContext.getAttendees(conference.id))
    }
  }

  const onBookingChange = async (email: string, packages: Package[], room: Partial<RoomOccupancy>) => {
    if (conference) {
      await attendeeContext.changeBooking(conference.id, email, { room, packages })
      setEditAttendee(null)
      setAttendees(await attendeeContext.getAttendees(conference.id))
    }
  }

  const attendeesMapper: RowMapper = row => {
    return <EventsAttendeesListRow
            key={row.index}
            attendee={row as Attendee}
            sharers={attendees.filter(a => !!a.booking).map(b => b.email) ?? []}
            onDelete={onDelete}
            onReset={onReset}
            onResend={onResend}
            onEdit={onEdit}
    />
  }


  const onClose = async () => {
    setMessageType(undefined)
    setMessage('')
  }

  const downloadCsv = async (conf: UUID) => {
    await attendeeContext.downloadCsv(conf)
  }
  const downloadStripe = async (conf: UUID) => {
    await attendeeContext.downloadStripe(conf)
  }

  return <><PagingTable rows={attendees}
          className={'attendees ' + (conference !== undefined ? '' : 'd-none')}
          listHeader={<AttendeesHeader/>}
          tableHeader={<AttendeesTableHeader/>}
          emptyRow={<EmptyEventsAttendeesListRow/>}
          rowMapper={attendeesMapper}
          exportCsv={conference ? () => downloadCsv(conference.id) : undefined}
          exportStripe={conference ? () => downloadStripe(conference.id) : undefined}
  />
    {messageType !== undefined &&
            <ModalMessage dialogAction={onClose} messageType={messageType} messageHtml={message}/>
    }
    {editAttendee &&
      <EventsAttendeeEditModal 
        attendee={editAttendee}
        onAttendeeDetailsChange={onAttendeeDetailsChanged}
        onBookingChange={onBookingChange}
        onClose={onAttendeeEditClose} />
    }
  </>
}