import {
  OverlayTrigger,
  Tooltip
} from 'react-bootstrap'
import React, {useRef} from 'react'
import {FaCheckCircle} from 'react-icons/all'

export const normalized = (str?: string) => str?.trim().toLowerCase() ?? ''
type DiversityStatusProps = {
  diversity: string
}
export const DiversityStatus = ({diversity}: DiversityStatusProps) => {
  const renderTooltip = (props: any) => (
          <Tooltip id="button-tooltip" {...props}>
            {diversity}
          </Tooltip>
  )
  const target = useRef(null)
  // noinspection RequiredAttributes
  const icon = normalized(diversity) === 'yes'
          ? <FaCheckCircle className={'text-success'}/>
          : normalized(diversity) === 'no' ? '' :
                  // @ts-ignore  Type of target attribute is inaccurate
                  <OverlayTrigger target={target.current}
                          placement="right"
                          delay={{show: 100, hide: 200}}
                          overlay={renderTooltip}
                  >
                    <div ref={target}><FaCheckCircle className={'text-warning'}/></div>
                  </OverlayTrigger>
  return <>{icon}</>
}