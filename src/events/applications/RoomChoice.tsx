import {
  Applicant,
  RoomType,
  RoomTypeName
} from '../../main/types'
import {
  Button,
  OverlayTrigger,
  Popover
} from 'react-bootstrap'
import React, {
} from 'react'
import {FaArrowRight} from 'react-icons/all'

import './RoomChoice.scss'

type RoomChoiceProps = {
  applicant: Applicant,
  roomTypes: RoomType[],
  onForwardAttendee: (roomType: RoomTypeName) => void,
  onForwardSponsor: (roomType: RoomTypeName) => void,
  onForwardStaff: (roomType: RoomTypeName) => void
}

export function RoomChoice({applicant, roomTypes, onForwardAttendee, onForwardStaff, onForwardSponsor}: RoomChoiceProps) {
  const popoverStaff =
          (<Popover id={'room-choice'} className="popover-positioned-top" title="Choose a room type">
            <Popover.Title as="h3">Select a room type:</Popover.Title>
            <Popover.Content>
              {roomTypes.map((r: RoomType, index: number) => <Button
                      key={index}
                      className="room"
                      onClick={() => {
                        onForwardStaff(r.type)
                      }}>{r.description}</Button>)}
            </Popover.Content>
          </Popover>)

  const popoverSponsor =
          (<Popover id={'room-choice'} className="popover-positioned-top" title="Choose a room type">
            <Popover.Title as="h3">Select a room type:</Popover.Title>
            <Popover.Content>
              {roomTypes.map((r: RoomType, index: number) => <Button
                      key={index}
                      className="room"
                      type={'primary'}
                      onClick={() => {
                        onForwardSponsor(r.type)
                      }}>{r.description}</Button>)}
            </Popover.Content>
          </Popover>)

  const popoverAttendee =
          (<Popover id={'room-choice'} className="popover-positioned-top" title="Choose a room type">
            <Popover.Title as="h3">Select a room type:</Popover.Title>
            <Popover.Content>
              {roomTypes.map((r: RoomType, index: number) => <Button
                      key={index}
                      className="room"
                      type={'primary'}
                      onClick={() => {
                        onForwardAttendee(r.type)
                      }}>{r.description}</Button>)}
            </Popover.Content>
          </Popover>)
  // noinspection RequiredAttributes
  return <>
    <OverlayTrigger
            trigger="click"
            placement={'top'}
            overlay={popoverAttendee}
            rootClose={true}
    >
      <Button className="pt-0 pb-1 pl-2 pr-2 ml-2" variant="warning" title={`Give ${applicant.name} a seat`}>
        <FaArrowRight/>
      </Button>
    </OverlayTrigger>
    <OverlayTrigger
            trigger="click"
            placement={'top'}
            overlay={popoverSponsor}
            rootClose={true}
    >
      <Button className="pt-0 pb-1 pl-2 pr-2 ml-2" variant="info" title={`Give ${applicant.name} a sponsored seat`}>
        <FaArrowRight/>
      </Button>
    </OverlayTrigger>
    <OverlayTrigger
            trigger="click"
            placement={'top'}
            overlay={popoverStaff}
            rootClose={true}
    >
      <Button className="pt-0 pb-1 pl-2 pr-2 ml-2" variant="success" title={`Give ${applicant.name} a staff seat`}>
        <FaArrowRight/>
      </Button>
    </OverlayTrigger>
  </>
}
