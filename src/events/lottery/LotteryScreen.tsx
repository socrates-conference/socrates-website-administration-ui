import React, {
  ChangeEventHandler,
  MouseEventHandler,
  useEffect,
  useState
} from 'react'
import {useConference} from '../../main/ConferenceProvider'
import {
  Booking,
  Conference,
  RoomType,
  RoomTypeName,
  Stats
} from '../../main/types'
import './LotteryScreen.scss'
import {
  Button,
  Form
} from 'react-bootstrap'
import {InputField} from '../../form/InputField'
import {useBooking} from '../../main/BookingProvider'
import {ensure} from '../../common/ensure'
import {useApplicants} from '../../main/ApplicantsProvider'
import {FormSubmitHandler} from '../../form/events'
import {HeadlessPagingTable} from '../../table/HeadlessPagingTable'
import {RowMapper} from '../../table/PagingTable'
import {
  EmptyLotteryBookingsListRow,
  LotteryBookingsListRow
} from './LotteryBookingsListRow'
import {ModalMessage} from '../../modal/ModalMessage'
import {MessageType} from '../../modal/messages'

const getTotalBedCount = (conference: Conference): number => conference?.roomTypes?.reduce((sum, r) => sum + r.beds * r.count, 0) ?? 0
const getBookedBedsCount = (stats: Stats): number => stats && stats.bookedBeds ? Object.values(stats.bookedBeds).reduce((sum, b) => sum + b ) :0
const getBookedBedsDiversityCount = (stats: Stats): number => stats && stats.bookedBedsDiversity ? Object.values(stats.bookedBedsDiversity).reduce((sum, n) => sum + n, 0): 0

const LotteryBookingsListTableHeader = () => <tr>
  <th>#</th>
  <th>Email</th>
  <th>Room type</th>
  <th>Diversity</th>
  <th>Actions</th>
</tr>

function getAvailableBedsTotal(conference: Conference, stats: Stats): string {
  const totalBedCount: number = getTotalBedCount(conference)
  const availableBedCount: number = totalBedCount - getBookedBedsCount(stats)
  return availableBedCount + '/' + totalBedCount
}

function getAssignedBedsDiversity(conference: Conference, stats: Stats): string {
  const totalBedCount: number = getTotalBedCount(ensure(conference))
  const diversityBedsCount: number = Math.round(totalBedCount * (conference.diversityRatio ?? 0) / 100 )
  const assignedBeds: number = getBookedBedsDiversityCount(ensure(stats))
  return assignedBeds + '/' + diversityBedsCount
}

function getRoomAvailableBeds(r: RoomType, stats: Stats | undefined): number {
  return stats && stats.bookedBeds ? (r.beds * r.count) - (stats?.bookedBeds[r.type] ?? 0) : 0
}

export function LotteryScreen() {
  const conferenceContext = useConference()
  const applicantContext = useApplicants()
  const shoppingContext = useBooking()

  const [conference, setConference] = useState<Conference | undefined>()
  const [stats, setStats] = useState<Stats>()
  const [beds, setBeds] = useState<Record<string, number>|undefined>(undefined)
  const [bookings, setBookings] = useState<Booking[] | undefined>(undefined)
  const [messageType, setMessageType] = useState<MessageType|undefined>(undefined)
  const [message, setMessage] = useState<string>('')

  useEffect(() => {
    let isSubscribed = true
    if (conference === undefined) {
      if (conferenceContext.current) setConference(conferenceContext.current)
      else {
        conferenceContext.choices()
                .then(c => isSubscribed && setConference(c[0]))

      }
    } else {
      if (!stats) {
        shoppingContext.getStats(conference.id)
                .then(s => {
                  setStats(s)
                })
                .catch(e => {
                  console.error(e)
                })
      } else if (beds === undefined) {
        const bds: Record<string, number> = conference.roomTypes?.reduce((b, r)=>{
          b[r.type] = getRoomAvailableBeds(r, stats)
          console.log("",b)
          return b
        },{} as Record<string, number>) ?? {}
        setBeds(bds)
      }
    }
  }, [conferenceContext, conference, stats, beds, shoppingContext])

  const onChange = (roomType: RoomTypeName): ChangeEventHandler<HTMLInputElement> => (event) => {
    const count = parseInt(event.target.value)
    const changed = {...beds, [roomType]: count}
    setBeds(changed)
  }

  const onSubmit: FormSubmitHandler = async (event) => {
    event.preventDefault()
    if (conference) {
      await applicantContext.runLottery(
              {
                conferenceId: conference.id,
                diversityPercentage: conference.diversityRatio ?? 0,
                bedsToAllocate: ensure(beds)
              }).then(bookings => setBookings(bookings))
    }
  }

  const onCancel: MouseEventHandler = async (event) => {
    event.preventDefault()
    await applicantContext.cancelLottery()
  }

  const onConfirm: MouseEventHandler = async (event) => {
    event.preventDefault()
    await applicantContext.executeLottery(ensure(bookings), ensure(conference?.id))
            .catch(e => {
              console.error('Outer:', e)
            })
            .then(async message => {
              console.log('success', message)
            })
  }

  const bookingsMapper: RowMapper = row => {
    return <LotteryBookingsListRow
            key={row.index}
            index={ensure(row.index, 'List rows must have an index.')}
            booking={row as Booking}
    />
  }

  const onClose = async() => {
    setMessageType(undefined)
    setMessage('')
  }

  return <div className={`container lottery ${conference && stats ? '' : 'd-none'}`}>
    <div className="row">
      <div className="col-12">
        <div className="card mt-4">
          <div className="row">
            <div className="col-12">
              <div className="card-header">
                <h5 className={'mt-2'}>Lottery</h5>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="card-body p-4">
                {conference && stats && <Form>
                  <div className={'row'}>
                    <h6 className={'ml-3 mb-4'}>Current stats</h6>
                  </div>
                  <div className={'row'}>
                    <div className={'col-6 pr-1'}>
                      <InputField className={'mb-2'} disabled label={'Attendees total'}
                              value={'' + ensure(stats).attendeeCount} placeholder={'0'}/>
                    </div>
                    <div className={'col-6 pl-1'}>
                      <InputField className={'mb-2'} disabled label={'Available beds total'}
                              value={'' + getAvailableBedsTotal(ensure(conference), ensure(stats))}
                              placeholder={'0'}/>
                    </div>

                    <div className={'col-6 pr-1'}>
                      <InputField className={'mb-2'} label={'Diversity Percentage'} disabled
                              value={'' + (conference.diversityRatio ?? 0)} placeholder={'40'}/>
                    </div>
                    <div className={'col-6 pl-1'}>
                      <InputField disabled label={'Assigned beds diversity'}
                              value={'' + getAssignedBedsDiversity(ensure(conference), ensure(stats))}
                              placeholder={'0'}/>
                    </div>
                  </div>
                  <hr/>
                  <div className={'row'}>
                    <h6 className={'ml-3 mb-4'}>Assign # of beds to give away</h6>
                  </div>
                  <div className={'row'}>
                    <div className={'col-3'}></div>
                    <div className={'col-6 pr-1'}>
                      {conference && beds && conference
                              .roomTypes?.map(r =>
                                      (<InputField
                                                      key={r.type}
                                                      className={'beds mb-2'}
                                                      label={r.description}
                                                      value={'' + ensure(beds)[r.type]}
                                                      placeholder={'Assign beds'}
                                                      onChange={onChange(r.type)}/>
                                      )
                              )}
                    </div>
                    <div className={'col-3'}></div>
                  </div>
                  <div className={'row mt-4 pl-0 pr-0'}>
                    <div className={'col-9'}></div>
                    <div className={'col-3 text-right'}>
                      <Button type={'submit'} variant={'info'} onClick={onSubmit}>Run lottery draw</Button>
                    </div>
                  </div>
                </Form>}
              </div>
            </div>
          </div>
          {bookings && <div>

          <hr/>
          <div className="row">
            <HeadlessPagingTable
                    className={'bookings'}
                    rows={bookings ?? []}
                    rowMapper={bookingsMapper}
                    emptyRow={<EmptyLotteryBookingsListRow/>}
                    tableHeader={<LotteryBookingsListTableHeader/>}
            />
          </div>
          <div className="row">
            <div className="col-12">
              <div className="card-footer p-4 text-right">
                <Button className="mr-1" variant={'danger'} onClick={onCancel}>Cancel this draw</Button>
                <Button className="ml-1" variant={'success'} onClick={onConfirm}>Confirm this draw</Button>
              </div>
            </div>
          </div>
          </div>}
        </div>

      </div>
    </div>
    {messageType !== undefined &&
            <ModalMessage dialogAction={onClose} messageType={messageType} messageHtml={message}/>
    }
  </div>
}